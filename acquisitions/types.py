from graphene_django import DjangoObjectType
from .models import Genre, BookSuggestion, BookOrder, AddVendor

class BookGenreType (DjangoObjectType):
    class Meta:
        model = Genre
        fields = '__all__'


class SuggestionType (DjangoObjectType):
    class Meta:
        model = BookSuggestion
        fields = ('title', 'author', 'card', 'price')


class OrderType (DjangoObjectType):
    class Meta:
        model = BookOrder
        fields = '__all__'


class VendorType (DjangoObjectType):
    class Meta:
        model = AddVendor
        fields = ('name', 'email')
