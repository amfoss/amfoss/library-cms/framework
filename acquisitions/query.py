import graphene
from .types import BookGenreType, SuggestionType, OrderType, VendorType
from .models import Genre, BookOrder, BookSuggestion, AddVendor

class Query(graphene.ObjectType):
    genres = graphene.List(BookGenreType)
    suggestions = graphene.List(SuggestionType)
    orders = graphene.List(OrderType)
    vendors = graphene.List(VendorType)
    vendor_by_name = graphene.Field(VendorType, name = graphene.String(required = True))
    order_by_status = graphene.Field(OrderType, status = graphene.String())

    def resolve_genres(root, info):
        return Genre.objects.all()

    def resolve_suggestions(root, info):
        return BookSuggestion.objects.all()

    def resolve_orders(root, info):
        return BookOrder.objects.select_related("vendor").all()

    def resolve_vendors(root, info):
        return AddVendor.objects.all()

    def resolve_vendor_by_name(root, info, name):
        try:
            return AddVendor.objects.get(name=name)
        except AddVendor.DoesNotExist:
            return Exception('Vendor doesn\'t exist')

    def resolve_order_by_status(root, info, status):
        try:
            return BookOrder.objects.get(orderStatus = status)
        except BookOrder.DoesNotExist:
            return Exception('Invalid Book Order')
